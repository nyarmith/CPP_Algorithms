```
ID  | Problem Name                         | Online Juge | Difficulty | CPP | Rust |
----+--------------------------------------+-------------+------------+-----+------+
1   | Young Physicist                      | Codeforces  | 1          |  x  |  x   |
2   | Beautiful Matrix                     | Codeforces  | 1          |  x  |  x   |
3   | Queue at the School                  | Codeforces  | 1          |  x  |  x   |
4   | Borze                                | Codeforces  | 1          |  x  |  x   |
5   | Beautiful Year                       | Codeforces  | 1          |  x  |  x   |
6   | Lights Out                           | Codeforces  | 1          |  x  |  x   |
7   | Word                                 | Codeforces  | 1          |  x  |  x   |
8   | Word Capitalization                  | Codeforces  | 1          |  x  |  x   |
9   | Nearly Lucky Number                  | Codeforces  | 1          |  x  |  x   |
10  | Stones on the Table                  | Codeforces  | 1          |  x  |  x   |
11  | Panoramix's Prediction               | Codeforces  | 1          |  x  |  x   |
12  | Ultra-Fast Mathematician             | Codeforces  | 1          |  x  |  x   |
13  | Perfect Permutation                  | Codeforces  | 1          |  x  |  x   |
14  | Arrival of the General               | Codeforces  | 1          |  x  |  x   |
15  | Drinks                               | Codeforces  | 1          |  x  |  x   |
16  | Insomnia cure                        | Codeforces  | 1          |  x  |  x   |
17  | Cupboards                            | Codeforces  | 1          |  x  |  x   |
18  | I_love_\%username\%                  | Codeforces  | 1          |  x  |  x   |
19  | Tram                                 | Codeforces  | 1          |  x  |  x   |
20  | Helpful Maths                        | Codeforces  | 1          |  x  |  x   |
21  | Is your horseshoe on the other hoof? | Codeforces  | 1          |  x  |  x   |
22  | Way Too Long Words                   | Codeforces  | 1          |  x  |  x   |
23  | Boy or Girl                          | Codeforces  | 1          |  x  |  x   |
24  | Amusing Joke                         | Codeforces  | 1          |  x  |  x   |
25  | Soft Drinking                        | Codeforces  | 1          |  x  |  x   |
26  | HQ9+                                 | Codeforces  | 1          |  x  |  x   |
27  | Petya and Strings                    | Codeforces  | 1          |  x  |  x   |
28  | Team                                 | Codeforces  | 1          |  x  |  x   |
29  | Bit++                                | Codeforces  | 1          |  x  |  x   |
30  | Effective Approach                   | Codeforces  | 2          |  x  |  x   |
31  | Dima and Friends                     | Codeforces  | 2          |  x  |  x   |
32  | Jzzhu and Children                   | Codeforces  | 2          |  x  |  x   |
33  | Supercentral Point                   | Codeforces  | 2          |  x  |  x   |
34  | Petr and Book                        | Codeforces  | 2          |  x  |  x   |
35  | Parallelepiped                       | Codeforces  | 2          |  x  |  x   |
36  | Reconnaissance 2                     | Codeforces  | 2          |  x  |  x   |
37  | Even Odds                            | Codeforces  | 2          |  x  |  x   |
38  | Little Elephant and Rozdil           | Codeforces  | 2          |  x  |  x   |
39  | Hexadecimal's theorem                | Codeforces  | 2          |  x  |  x   |
40  | Jeff and Digits                      | Codeforces  | 2          |  x  |  x   |
41  | Xenia and Ringroad                   | Codeforces  | 2          |  x  |  x   |
42  | Magic Numbers                        | Codeforces  | 2          |  x  |  x   |
43  | Translation                          | Codeforces  | 2          |  x  |  x   |
44  | Football                             | Codeforces  | 2          |  x  |  x   |
45  | Bicycle Chain                        | Codeforces  | 2          |  x  |      |
46  | Sale                                 | Codeforces  | 2          |  x  |      |
47  | System of Equations                  | Codeforces  | 2          |  x  |      |
48  | Business trip                        | Codeforces  | 2          |  x  |      |
49  | Dubstep                              | Codeforces  | 2          |  x  |      |
50  | k-String                             | Codeforces  | 2          |  x  |      |
51  | The number of positions              | Codeforces  | 2          |  x  |      |
52  | Football                             | Codeforces  | 2          |  x  |      |
53  | String Task                          | Codeforces  | 2          |  x  |      |
54  | Little Elephant and Function         | Codeforces  | 2          |  x  |      |
55  | Present from Lena                    | Codeforces  | 2          |  x  |      |
56  | Dragons                              | Codeforces  | 2          |  x  |      |
57  | Puzzles                              | Codeforces  | 2          |  x  |      |
58  | Chat room                            | Codeforces  | 2          |  x  |      |
59  | Airport                              | Codeforces  | 2          |  x  |      |
60  | DZY Loves Chessboard                 | Codeforces  | 3          |  x  |      |
61  | Pashmak and Flowers                  | Codeforces  | 3          |  x  |      |
62  | Jeff and Periods                     | Codeforces  | 3          |  x  |      |
63  | Little Girl and Game                 | Codeforces  | 3          |  x  |      |
64  | Sail                                 | Codeforces  | 3          |  x  |      |
65  | Shower Line                          | Codeforces  | 3          |  x  |      |
66  | Shooshuns and Sequence               | Codeforces  | 3          |  x  |      |
67  | Xenia and Divisors                   | Codeforces  | 3          |  x  |      |
68  | Letter                               | Codeforces  | 3          |  x  |      |
69  | Kitahara Haruki's Gift               | Codeforces  | 3          |  x  |      |
70  | Comparing Strings                    | Codeforces  | 3          |  x  |      |
71  | Hungry Sequence                      | Codeforces  | 3          |  x  |      |
72  | Big Segment                          | Codeforces  | 3          |  x  |      |
73  | Little Elephant and Bits             | Codeforces  | 3          |  x  |      |
74  | Yaroslav and Permutations            | Codeforces  | 3          |  x  |      |
75  | Fence                                | Codeforces  | 3          |  x  |      |
76  | TL                                   | Codeforces  | 3          |  x  |      |
77  | Increase and Decrease                | Codeforces  | 3          |  x  |      |
78  | Two Bags of Potatoes                 | Codeforces  | 3          |  x  |      |
79  | Unlucky Ticket                       | Codeforces  | 3          |  x  |      |
80  | Boys and Girls                       | Codeforces  | 3          |  x  |      |
81  | Easy Number Challenge                | Codeforces  | 3          |  x  |      |
82  | Pythagorean Theorem II               | Codeforces  | 3          |  x  |      |
83  | Cards with Numbers                   | Codeforces  | 3          |  x  |      |
84  | Domino                               | Codeforces  | 3          |  x  |      |
85  | Cinema Line                          | Codeforces  | 3          |  x  |      |
86  | Rank List                            | Codeforces  | 3          |  x  |      |
87  | Cut Ribbon                           | Codeforces  | 3          |  x  |      |
88  | IQ Test                              | Codeforces  | 3          |  x  |      |
89  | Building Permutation                 | Codeforces  | 3          |  x  |      |
90  | Kuriyama Mirai's Stones              | Codeforces  | 3          |  x  |      |
91  | T-primes                             | Codeforces  | 3          |  x  |      |
92  | Sereja and Suffixes                  | Codeforces  | 3          |  x  |      |
93  | Flipping Game                        | Codeforces  | 3          |  x  |      |
94  | Free Cash                            | Codeforces  | 3          |  x  |      |
95  | Polo the Penguin and Matrix          | Codeforces  | 4          |  x  |      |
96  | Jzzhu and Sequences                  | Codeforces  | 4          |  x  |      |
97  | Appleman and Card Game               | Codeforces  | 4          |  x  |      |
98  | Sort the Array                       | Codeforces  | 4          |  x  |      |
99  | Sereja and Bottles                   | Codeforces  | 4          |  x  |      |
100 | Adding Digits                        | Codeforces  | 4          |  x  |      |
```
