use std::io;

// number is magic if it is composed of concatenations of 1, 14 and 144
fn is_magic(num : &str) -> bool {
    if num.len() == 0 {
        return true;
    }

    if num.len() >= 1 && &num[0..1] == "1" && is_magic(&num[1..]) {
        return true;
    }
    if num.len() >= 2 && &num[0..2] == "14" && is_magic(&num[2..]) {
        return true;
    }
    if num.len() >= 3 && &num[0..3] == "144" && is_magic(&num[2..]) {
        return true;
    }
    return false;
}

fn main() {
    let handle = io::stdin();
    let mut buf = String::new();
    handle.read_line(&mut buf).unwrap();
    if is_magic(buf.trim()) {
        println!("YES");
    } else {
        println!("NO");
    }
}
