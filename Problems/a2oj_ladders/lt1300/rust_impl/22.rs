use std::io;

fn main() {
    let handle = io::stdin();
    let mut buf = String::new();

    handle.read_line(&mut buf).unwrap();
    let n = buf.trim().parse::<u8>().unwrap();
    buf.clear();

    let mut strs = Vec::<String>::new();

    for _ in 0..n {
        handle.read_line(&mut buf).unwrap();
        let mut substr = buf.trim().to_string();
        if substr.len() > 10 {
            let mut chars = substr.chars();
            substr = format!("{}{}{}", chars.next().unwrap(), (substr.len() - 2).to_string(), chars.last().unwrap());
        }
        strs.push(substr);
        buf.clear();
    }

    for s in strs {
        println!("{}", s);
    }
}
