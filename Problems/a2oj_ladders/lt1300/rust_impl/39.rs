use std::io;

fn main() {
    let handle = io::stdin();
    let mut buf = String::new();
    handle.read_line(&mut buf).unwrap();
    let n : usize = buf.trim().parse().unwrap();

    let (mut a, mut b, mut c) = (0, 1, 1);

    if n == 0 {
        println!("0 0 0");
    }
    else if n == 1 {
        println!("0 0 1");
    } else {
        while b + c != n { (a, b, c) = (b, c, b + c); }
        println!("{} {} {}", a, b, b);
    }
}
