use std::io;

fn main() {
    let mut buf = String::new();
    let handle = io::stdin();
    handle.read_line(&mut buf).unwrap();
    for c in buf.trim().chars() {
        match c {
            'H' => {println!("YES"); return;},
            'Q' => {println!("YES"); return;},
            '9' => {println!("YES"); return;},
            _ => continue
        }
    }
    println!("NO");
}
