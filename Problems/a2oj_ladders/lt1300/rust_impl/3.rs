use std::io;

fn advance(s: &mut Vec<char>) {
    let mut iter = (0..s.len()-1).rev();
    while let Some(i) = iter.next() {
        match (s[i],s[i+1]) {
            ('B', 'G') => {
                s[i] = 'G';
                s[i+1] = 'B';
                iter.next();    // don't allow double skip for same 'G'
            },
            _ => {},
        }
    }
}

fn main() -> io::Result<()> {
    let handle = io::stdin();

    let mut buf  = String::new();
    handle.read_line(&mut buf)?;


    // split first line into n and t
    let mut iter =  buf.trim().split_whitespace();

    let _n = iter.next().unwrap().parse::<u8>().unwrap(); //unused
    let t = iter.next().unwrap().parse::<u8>().unwrap();

    buf.clear();

    // read order input
    handle.read_line(&mut buf)?;


    let mut line : Vec<char> = buf.trim().chars().collect();

    for _i in 0..t {
        advance(&mut line);
    }

    let out : String = line.into_iter().collect();
    println!("{}",out);
    
    Ok(())
}
