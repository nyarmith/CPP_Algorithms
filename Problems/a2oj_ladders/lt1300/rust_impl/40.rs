use std::io;

fn main() {
    let handle = io::stdin();
    let mut buf = String::new();
    handle.read_line(&mut buf).unwrap();
    let n : usize = buf.trim().parse().unwrap();

    buf.clear();
    handle.read_line(&mut buf).unwrap();
    let (mut fives, mut zeros) = (0, 0);

    for v in buf.split_whitespace().map(|s| s.parse::<usize>().unwrap()) {
        if v == 5 {
            fives += 1
        } else {
            zeros += 1;
        }
    }

    if fives >= 9 && zeros > 0 {
        let mut res = "".to_string();
        while fives >= 9 {
            fives -= 9;
            res += "555555555";
        }
        while zeros > 0 {
            zeros -= 1;
            res += "0";
        }
        println!("{}", res);
    } else if zeros > 0 {
        println!("0");
    } else {
        println!("-1");
    }
}
