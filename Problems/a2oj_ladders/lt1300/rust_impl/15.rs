use std::io;

fn main() {
    let mut buf = String::new();
    let handle = io::stdin();
    handle.read_line(&mut buf).expect("whoops");
    buf.clear();
    handle.read_line(&mut buf).expect("whoops2");

    let nums : Vec<u8> = buf.trim().split(" ").map(|s| s.parse::<u8>().unwrap()).collect();

    // just averaging
    let avg : f32 = nums.iter().map(|x| (*x as f32)).sum::<f32>() / (nums.len() as f32);
    println!("{}", avg);
}
