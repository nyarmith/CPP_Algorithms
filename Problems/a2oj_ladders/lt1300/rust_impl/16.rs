use std::io;
use std::convert::TryInto;
use std::collections::HashSet;

fn main() -> io::Result<()> {
    let mut buf = String::new();
    let handle = io::stdin();
    for _ in 0..5 {
        handle.read_line(&mut buf)?;
    }
    let [k, l, m, n, d] : [usize; 5] = buf.split_whitespace().map(|s| s.parse::<usize>().unwrap()).collect::<Vec<usize>>().try_into().unwrap();

    if [k, l, m, n].contains(&1) {
        println!("{}", d);
        return Ok(());
    }

    let mut nums = HashSet::new();

    for s in [k, l, m, n] {
        for x in (s..=d).step_by(s) {
            nums.insert(x);
        }
    }

    println!("{}", nums.len());

    Ok(())
}
