use std::io;
//use std::vec;

// ok... basic new challenge is to read lines into a 2d array
// this time we'll read lines
fn main() -> io::Result<()> {
    let handle = io::stdin();
    let mut mat = [[0u8; 5]; 5];

    // we know the shape of this input ahead of time
    //let mut row = 0;
    let mut lines = handle.lines();

    let mut sp_x = 0i8;
    let mut sp_y = 0i8;

    for i in 0..5 {
        let line = lines.next().unwrap()?;
        // this doesn't work as I want because mapped iterators are lazy and don't evaluate unless
        // used! Great warning, compiler!
        // line.unwrap()?.trim().split_whitespace().map(|x| {mat[i][j] = x.parse::<u8>().unwrap(); j += 1});
        // TODO: Handle rest of htis corerctly
        let mut iter_obj = line.trim().split_whitespace();
        for j in 0..5 {
            mat[i][j] = iter_obj.next().unwrap().parse::<u8>().unwrap();
            if mat[i][j] == 1 {
                sp_x = i as i8;
                sp_y = j as i8;
            }
        }
    }

    //for x in 0..5 {
    //    for y in 0..5 {
    //        print!("{} ", mat[x][y]);
    //    }
    //    print!("\n");
    //}

    print!("{}\n",(2i8 - sp_x).abs() + (2i8 - sp_y).abs());

    Ok(())
}
