use std::io;

fn main() {
    let handle = io::stdin();
    let mut buf = String::new();
    handle.read_line(&mut buf).unwrap();
    let n  = buf.trim().parse::<usize>().unwrap();

    let mut x = 0;
    buf.clear();
    for _ in 0..n {
        handle.read_line(&mut buf).unwrap();
        match buf.find("++") {
            Some(_) => x += 1,
            _ => x -= 1,
        }
        buf.clear();
    }

    println!("{}", x);
}
