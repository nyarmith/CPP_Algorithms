use std::io;
use std::iter::zip;

fn lex_cmp(s1 : &str, s2 : &str) -> i8 {
    let cmp_iter = zip(s1.chars(), s2.chars());
    for (c1, c2) in cmp_iter {
        if c1 < c2 {
            return -1;
        } else if c1 > c2 {
            return 1;
        }
    }
    0
}

fn main() {
    let mut str1 = String::new();
    let mut str2 = String::new();
    let handle = io::stdin();
    handle.read_line(&mut str1).unwrap();
    handle.read_line(&mut str2).unwrap();

    str1 = str1.trim().to_lowercase().to_string();
    str2 = str2.trim().to_lowercase().to_string();

    //println!("{}", lex_cmp(&str1, &str2));
    if let Some(n) = zip(str1.chars(), str2.chars()).find_map(|(a,b)| if a>b { Some(1) } else if a<b { Some(-1) } else {None}) {
        println!("{}",n);
    } else{
        println!("0");
    }
}
