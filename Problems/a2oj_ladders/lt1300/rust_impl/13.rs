use std::io;

fn main() {
    let mut buf = String::new();
    let handle = io::stdin();
    handle.read_line(&mut buf).expect("whoops");
    let N : usize = buf.trim().parse().expect("parse fail");
    if N %2 == 1 {
        println!("-1");
    } else {
        for i in (0..N).step_by(2) {
            let i = i+1; // indexing is from 1
            print!("{} {} ", i+1, i);
        }
        println!("");
    }
}
