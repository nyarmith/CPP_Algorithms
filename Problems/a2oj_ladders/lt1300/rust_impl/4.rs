use std::io;

fn main() -> io::Result<()> {
    // decoding a single string as a borze code
    let handle = io::stdin();
    let mut buf = String::new();
    handle.read_line(&mut buf)?;

    let input : Vec<char> = buf.trim().chars().collect();

    let mut ret : u32 = 0;

    // a is either '.' or '-.' or '--', for 0, 1, 2 respectively
    let mut ind = 0;
    while ind < input.len() {
        match input[ind] {
            '.' => {
                ret *=3;
                ind += 1;
            }
            _ => { 
                ind += 1;
                match input[ind] {
                    '.' => ret += 1,
                    _ => ret += 2
                }
                ind += 1;
            }
        }
    }

    println!("{}",ret);
    Ok(())
}
