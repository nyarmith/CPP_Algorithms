use std::io;

fn main() {
    let handle = io::stdin();

    let mut buf = String::new();
    handle.read_line(&mut buf).unwrap();
    // SOLN 1
    //let mut nums : Vec<usize> = buf.trim().split("+").map(|s| s.parse::<usize>().unwrap()).collect();
    //nums.sort();
    // TODO: Deep dive into the ownership stuff happening here w/ a cognitive tool
    // ok, to_string, the ToString trait, automatically implemented for any type implementing the
    // Display traiy, returns a String, so it will make new memory on the heap here, and I don't
    // think there's a more magically memory efficient way to do this, other than to work with the
    // input buffer (maybe should give that a shot)
    //let nums : Vec<String> = nums.iter().map(|s| s.to_string()).collect();
    //let out_str = nums.join("+");

    // SOLN 2
    let mut nums : Vec<&str> = buf.trim().split("+").collect();
    nums.sort_by(|s1, s2| s1.parse::<u32>().unwrap().cmp(&s2.parse::<u32>().unwrap()));
    let out_str = nums.join("+");
    println!("{}", out_str);
}
