use std::io;

fn main() {
    let mut buf = String::new();
    let handle = io::stdin();
    handle.read_line(&mut buf).unwrap();
    let n = buf.trim().parse::<usize>().unwrap();
    buf.clear();

    let mut p = 0;
    for _ in 0..n {
        handle.read_line(&mut buf).unwrap();
        if buf.split_whitespace().map(|s| s.parse::<usize>().unwrap()).sum::<usize>() > 1 {
            p += 1;
        }
        buf.clear();
    }
    println!("{}", p);
}
