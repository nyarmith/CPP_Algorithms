use std::io;

fn main() {
    let mut buf = String::new();
    let handle = io::stdin();
    handle.read_line(&mut buf).expect("oops failure reading first line");
    buf.clear();
    handle.read_line(&mut buf).expect("oops failure reading second line");
    let line = buf.trim().as_bytes();

    let stones_to_remove : usize = line.windows(2).map(|w| (w[0] == w[1]) as usize).sum();
    println!("{}", stones_to_remove);
}
