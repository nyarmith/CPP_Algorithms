use std::io;

fn main() {
    let handle = io::stdin();
    let mut buf = String::new();
    handle.read_line(&mut buf).unwrap();
    let n : usize = buf.trim().parse().unwrap();
    buf.clear();
    handle.read_line(&mut buf).unwrap();
    let mut total : usize = 0;
    let nums : Vec<usize> = buf.split_whitespace().map(|s| {
        let my_num = s.parse::<usize>().unwrap();
        total += my_num;
        my_num})
        .collect();

    let mut n = n % total;
    for (i, v) in nums.into_iter().enumerate() {
        if n < v {
            println!("{}", i+1);
            break;
        } else {
            n -= v;
        }
    }
}
