use std::io;

fn main() {
    let mut buf = String::new();
    let handle = io::stdin();

    handle.read_line(&mut buf).unwrap();

    let mut chars : Vec<char> = buf.trim().chars().collect();
    chars.sort();
    chars.dedup();

    match chars.len() % 2 {
        1 => println!("IGNORE HIM!"),
        _ => println!("CHAT WITH HER!")
    }
}
