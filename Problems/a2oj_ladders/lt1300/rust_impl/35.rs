use std::io;
use std::convert::TryInto;

fn main() {
    let handle = io::stdin();
    let mut buf = String::new();

    handle.read_line(&mut buf).unwrap();
    let [a, b, c] : [usize; 3] = buf.split_whitespace().map(|s| s.parse::<usize>().unwrap()).collect::<Vec<usize>>().try_into().unwrap();

    let x2 = (b*c)/a;
    let y2 = (a*b)/c;
    let z2 = (a*c)/b;
    let x = f32::sqrt(x2 as f32) as u32;
    let y = f32::sqrt(y2 as f32) as u32;
    let z = f32::sqrt(z2 as f32) as u32;

    println!("{}", 4*(x+y+z));
}
