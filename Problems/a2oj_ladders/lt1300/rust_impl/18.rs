use std::io;
use std::convert::TryInto;

fn main() -> io::Result<()> {
    let handle = io::stdin();
    let mut buf = String::new();
    handle.read_line(&mut buf)?;
    let n : usize = buf.trim().parse().unwrap();
    buf.clear();
    handle.read_line(&mut buf)?;
    let arr : Vec<usize> = buf.trim().split_whitespace().map(|s| s.parse::<usize>().unwrap()).collect();

    let mut min = arr[0];
    let mut max = arr[0];
    let mut amazing = 0;

    for x in arr[1..].iter() {
        if *x < min {
            min = *x;
            amazing += 1;
        }
        else if *x > max {
            max = *x;
            amazing += 1;
        }
    }

    println!("{}", amazing);

    Ok(())
}
