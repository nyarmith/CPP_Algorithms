use std::io;
use std::cmp;

fn main() -> io::Result<()>{

    let mut buf = String::new();
    let handle = io::stdin();

    handle.read_line(&mut buf)?;
    let n : usize = buf.trim().parse().unwrap();
    buf.clear();

    let mut max: usize = 0;
    let mut cur : usize = 0;
    for _ in 0..n {
        handle.read_line(&mut buf)?;
        let mut iter = buf.trim().split_whitespace();
        let leave : usize = iter.next().unwrap().parse().unwrap();
        let enter : usize = iter.next().unwrap().parse().unwrap();
        buf.clear();

        cur -= leave;
        cur += enter;
        max = cmp::max(cur, max);
    }

    println!("{}", max);
    Ok(())
}
