use std::io;

fn main() {
    let mut buf = String::new();
    let handle = io::stdin();
    handle.read_line(&mut buf).unwrap();
    let mut cols : Vec<&str> = buf.trim().split_whitespace().collect();
    cols.sort();
    cols.dedup();
    println!("{}", 4-cols.len());
}
