use std::io;
use std::collections::HashMap;

fn main() {
    let mut buf = String::new();
    let handle = io::stdin();

    handle.read_line(&mut buf).unwrap();
    buf.clear();
    handle.read_line(&mut buf).unwrap();

    let mut map : HashMap<usize, usize> = HashMap::new();
    let mut min : usize = 1000000001;
    let mut min_i : usize = 0;
    let iter = buf.split_whitespace().map(|s| s.parse::<usize>().unwrap());
    
    for (i,v) in iter.enumerate() {
        if v < min {
            min = v;
            min_i = i;
        }
        *map.entry(v).or_default() += 1;
    }

    if *map.get(&min).unwrap() == 1 {
        println!("{}",  min_i+1);
    } else {
        println!("Still Rozdil");
    }
}
