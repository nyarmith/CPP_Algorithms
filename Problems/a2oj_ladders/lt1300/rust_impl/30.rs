use std::io;

fn main() {
    let mut buf = String::new();
    let handle = io::stdin();
    handle.read_line(&mut buf).unwrap();
    let n : usize = buf.trim().parse().unwrap();
    buf.clear();
    let mut ind_map : [usize; 100001] = [0; 100001];

    handle.read_line(&mut buf).unwrap();
    let en_iter = buf.trim().split_whitespace().map(|s| s.parse::<usize>().unwrap())
        .enumerate();

    for (i,x) in en_iter {
        ind_map[x] = i;
    }

    buf.clear();

    let mut l_c = 0;
    let mut r_c = 0;
    handle.read_line(&mut buf).unwrap();
    buf.clear();
    handle.read_line(&mut buf).unwrap();
    let nums = buf.trim().split_whitespace().map(|s| s.parse::<usize>().unwrap());

    for num in nums {
        l_c += ind_map[num] + 1;
        r_c += n - ind_map[num];
    }
    println!("{} {}", l_c, r_c);
}
