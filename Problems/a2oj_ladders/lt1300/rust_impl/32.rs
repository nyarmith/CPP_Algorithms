use std::io;
use std::convert::TryInto;

fn main() {
    let mut buf = String::new();
    let handle = io::stdin();
    handle.read_line(&mut buf).unwrap();
    let [_n,m] : [usize;2] = buf.split_whitespace().map(|s| s.parse::<usize>().unwrap()).collect::<Vec<usize>>().try_into().unwrap();
    buf.clear();
    handle.read_line(&mut buf).unwrap();
    let nums = buf.split_whitespace().map(|s| s.parse::<usize>().unwrap());
    let mut ind = 0;
    let mut max = 0;
    for (i, num) in nums.enumerate() {
        if (num-1) / m >= max {
            ind = i;
            max = (num-1) / m;
        }
    }

    println!("{}", ind+1);
}
