use std::io;
use std::cmp::min;

fn main() -> io::Result<()>{
    let mut buf = String::new();
    let handle = io::stdin();

    handle.read_line(&mut buf)?;
    let N : usize = buf.trim().parse().unwrap();

    let mut left = 0;
    let mut right = 0;
    for _ in 0..N {
        buf.clear();
        handle.read_line(&mut buf)?;
        match buf.trim() {
            "0 1" => {left+=0; right+=1;},
            "1 1" => {left+=1; right+=1;},
            "1 0" => {left+=1; right+=0;},
            "0 0" => {left+=0; right+=0;},
            _ => println!("ERROR, unhandled case: {}", buf)
        }
    }

    println!("{}", min(left,N-left) + min(right,N-right));

    Ok(())
}
