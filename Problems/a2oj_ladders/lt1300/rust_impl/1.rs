// I guess how we usually get stdio
use std::io;
use std::vec;

// our entrypoint
fn main() -> io::Result<()>{
    // explicitly declare our buffer
    let mut buffer = String::new();
    let handle = io::stdin();

    let mut my_vec = vec![0,0,0];

    // read first line
    handle.read_line(&mut buffer)?;

    let num_inputs = buffer.trim().parse::<u32>().unwrap();

    // now clear our buffer after writing to it
    buffer.clear();

    // repeatedly reading into same buffer then parsing
    for _line_num in 0..num_inputs {
        handle.read_line(&mut buffer)?;
        //let nums : Vec<i32> = buffer.split_whitespace().map(|s| s.parse::<i32>().unwrap()).collect();
        let mut i = 0;
        for s in buffer.trim().split_whitespace() {
            my_vec[i] += s.parse::<i32>().unwrap();
            i += 1;
        }

        // clear buffer for next line (no append!!)
        buffer.clear();
    }

    let mut inert = true;
    for x in my_vec.iter() {
        // we're borrowing the values, so they're reference/pointer types we're dereferenceing
        if *x != 0 {
            inert = false;
        }
    }

    if inert == true {
        print!("YES\n");
    } else {
        print!("NO\n");
    }

    // a good result
    Ok(())
}
