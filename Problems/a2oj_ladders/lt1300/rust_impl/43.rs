use std::io;

fn main() {
    let handle = io::stdin();
    let (mut str1, mut str2) = (String::new(), String::new());
    handle.read_line(&mut str1).unwrap();
    handle.read_line(&mut str2).unwrap();

    if str1.trim().chars().eq(str2.trim().chars().rev()) {
        println!("YES");
    } else {
        println!("NO");
    }
}
