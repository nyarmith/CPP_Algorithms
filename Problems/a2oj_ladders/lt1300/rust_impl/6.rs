// Lights Out

use std::io;

// getting a 3x3 grid of numbers whose values are between [0,100]

fn toggle_spread(mat: &mut Vec<Vec<i32>>, x_0 : isize, y_0: isize) {


    let offsets = [-1, 1];
    let check = |x_n, y_n| x_n >= 0 && x_n <= 2 && y_n >=0 && y_n<= 2;

    mat[x_0 as usize][y_0 as usize] += 1;
    for off_x in offsets.iter() {
        let x = x_0 + off_x;
        let y = y_0;

        if check(x, y) {
            mat[x as usize][y as usize]+= 1
        }
    }
    for off_y in offsets.iter() {
        let x = x_0;
        let y = y_0 + off_y;

        if check(x, y) {
            mat[x as usize][y as usize]+= 1
        }
    }
}

fn main() -> io::Result<()> {
    let mut mat : Vec<Vec<i32>> = Vec::new();
    let stdin = io::stdin();
    for line in stdin.lines().take(3) {
        // idk what, something like this, study this a bit next time
        let row: Vec<i32> = line.unwrap().split_whitespace()
            .map(|num| num.parse::<i32>().unwrap() % 2)
            .collect();
        mat.push(row);
    }


    // "process" adjacent lights
    let mut toggle_list : Vec<Vec<i32>> = Vec::new();
    for x in 0..=2 {
        for y in 0..=2 {
            if mat[x][y] == 1 {
                toggle_list.push(vec![x as i32,y as i32]);
            }
            mat[x][y] = 0;
        }
    }

    for pt in toggle_list.iter() {
        toggle_spread(&mut mat, pt[0] as isize, pt[1] as isize);
    }

    // now, any lights positions with odd number are off, and any with even number are on
    mat.iter_mut().flat_map(|row| row.iter_mut()).for_each(|elem| *elem = (*elem + 1) % 2);

    for x in 0..=2 {
        for y in 0..=2 {
            // ignore this because they like bad formatting
            //if y != 0 {
            //    print!(" ");
            //}
            print!("{}", mat[x][y]);
        }
        print!("\n");
    }

    Ok(())
}
