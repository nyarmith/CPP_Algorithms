use std::io;

// is the input a nearly lucky number? akak the number of lucky digits is a lucky number
// a lucky number is one whose decimal representation contains only the lucky digits 4 and 7

fn main() -> io::Result<()> {
    let mut buf = String::new();
    let handle = io::stdin();
    handle.read_line(&mut buf).expect("Error reading line!");
    let mut final_num : usize = buf.trim().chars().map(|c| (c=='4' || c=='7') as usize).sum();

    if final_num == 0 {
        println!("NO");
        return Ok(());
    }
    while final_num > 0 {
        let rm = final_num % 10;
        if rm != 4 && rm != 7 {
            println!("NO");
            return Ok(());
        }
        final_num /= 10;
    }
    println!("YES");

    Ok(())
}
