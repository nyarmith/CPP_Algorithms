use std::io;

// -- get min difference of soldiers in set, not the 
// actual problem we were asked to do but a good exercise
fn min_soldier_diff_whole_set(heights: Vec<usize>) -> (usize, usize) {
    let n = heights.len();
    // here we want to sort and then have the map to indices
    let mut idx : Vec<usize> = (0..n).collect();

    idx.sort_by(|a,b| heights[*a].cmp(&heights[*b]));

    let mut min_diff = 1000;
    let mut min_diff_idx = (0, 0);
    for idxs in idx.windows(2) {
        if heights[idxs[1]] - heights[idxs[0]] < min_diff {
            min_diff = heights[idxs[1]] - heights[idxs[0]];
            min_diff_idx = (idxs[1], idxs[0]);
        }
    }

    min_diff_idx
}

fn main() {
    let mut buf = String::new();
    let handle = io::stdin();
    handle.read_line(&mut buf).unwrap();
    let n : usize = buf.trim().parse().unwrap();
    buf.clear();
    handle.read_line(&mut buf).unwrap();
    let vals : Vec<i32> = buf.split_whitespace().map(|s| s.parse::<i32>().unwrap()).collect();

    let mut min_diff = (vals[0] - vals.last().unwrap()).abs();
    let mut min_diff_idx = (0, n-1);

    for i in 0..n-1 {
        let d = (vals[i] - vals[i+1]).abs();
        if d < min_diff {
            min_diff = d;
            min_diff_idx = (i, i+1);
        }
    }

    // indeces are expected via natural numbering
    println!("{} {}", min_diff_idx.0 + 1, min_diff_idx.1 + 1);
}
