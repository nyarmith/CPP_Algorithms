use std::io;

fn main() {
    let mut buf = String::new();
    let handle = io::stdin();
    handle.read_line(&mut buf).expect("nope");
    buf.clear();
    handle.read_line(&mut buf).expect("nope");
    let order : Vec<usize> = buf.trim().split_whitespace().map(|x| x.parse::<usize>().unwrap()).collect();

    let (mut min, mut min_i) = (101 as usize, 0 as usize);
    let (mut max, mut max_i) = (0 as usize, 0 as usize);

    for (i, num) in order.iter().enumerate() {
        if *num <= min {
            min = *num;
            min_i = i;
        }
        if *num > max {
            max = *num;
            max_i = i;
        }
    }


    let swaps = max_i + (order.len() - min_i - 1);
    let swaps = if min_i < max_i { swaps - 1 } else { swaps };
    println!("{}", swaps);

}
