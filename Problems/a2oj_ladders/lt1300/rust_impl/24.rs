use std::io;

fn main() {
    let (mut guest, mut host, mut address) = (String::new(), String::new(), String::new());
    let handle = io::stdin();
    handle.read_line(&mut guest).unwrap();
    handle.read_line(&mut host).unwrap();
    handle.read_line(&mut address).unwrap();

    guest = guest.trim().to_string();
    host = host.trim().to_string();
    address = address.trim().to_string();
    // address must consist of reordered letters from guest and host, but no more
    let mut letters1 : Vec<char> = guest.chars().collect();
    letters1.extend(host.chars());
    let mut letters2 : Vec<char> = address.chars().collect();

    letters1.sort();
    letters2.sort();

    if letters1 == letters2 {
        println!("YES");
    } else {
        println!("NO");
    }
}
