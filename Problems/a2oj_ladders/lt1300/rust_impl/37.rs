use std::io;

fn main() {
    let handle = io::stdin();
    let mut buf = String::new();
    handle.read_line(&mut buf).unwrap();
    let mut iter = buf.split_whitespace();
    let n : usize = iter.next().unwrap().parse().unwrap();

    // oh, we want to know what num is in position k, rather than what position k is in
    let k : usize = iter.next().unwrap().parse::<usize>().unwrap() - 1;

    let bound = n/2 + n%2;
    if k < bound {
        println!("{}", 1+2*k);
    } else {
        let k = k - bound + 1;
        println!("{}", 2*k);
    }
}
