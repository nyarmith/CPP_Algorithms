use std::io;
use std::cmp;
use std::convert::TryInto;

fn main() {
    let mut buf = String::new();
    let handle = io::stdin();
    handle.read_line(&mut buf).unwrap();
    let [n, k, l, c, d, p, nl, np] : [usize; 8] = 
        buf.trim().split_whitespace()
        .map(|s| s.parse::<usize>().unwrap())
        .collect::<Vec<usize>>()
        .try_into().unwrap();

    let vol = k*l;
    let slices = c*d;
    let drinks = cmp::min(cmp::min(slices, vol/nl), p/np);
    let toasts = drinks/n;
    println!("{}",toasts);
}
