use std::io;
use std::str::FromStr;

struct Pt {
    x : i32,
    y : i32
}

#[derive(Debug, PartialEq, Eq)]
struct ParsePtError;

impl Pt {
    fn new(x: i32, y: i32) -> Self {
        Pt {x, y}
    }
}

impl FromStr for Pt {
    type Err = ParsePtError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (x, y) : (i32, i32);
        let mut iter = s.split_whitespace();
        if let Some(n) = iter.next() {
            x = n.parse::<i32>().unwrap();
        } else {
            return Err(ParsePtError{});
        }
        if let Some(n) = iter.next() {
            y = n.parse::<i32>().unwrap();
        } else {
            return Err(ParsePtError{});
        }
        Ok(Pt{x, y})
    }
}

fn main() {
    let handle = io::stdin();
    let mut buf = String::new();
    handle.read_line(&mut buf).unwrap();
    let mut pts : Vec<Pt> = Vec::new();
    let n = buf.trim().parse::<usize>().unwrap();
    buf.clear();
    for _ in 0..n {
        handle.read_line(&mut buf).unwrap();
        pts.push(buf.parse::<Pt>().unwrap());
        buf.clear();
    }
    let (mut left, mut right, mut top, mut bottom) = (0, 0, 0, 0);
    for pt in &pts {
        if pt.x < left {
            left = pt.x;
        }
        if pt.x > right {
            right = pt.x;
        }
        if pt.y < top {
            top = pt.y;
        }
        if pt.y > bottom {
            bottom = pt.y;
        }
    }

    println!("{}", pts.iter().filter(|p|  left < p.x && p.x < right && top < p.y && p.y < bottom).count());
}
