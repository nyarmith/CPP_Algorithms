use std::io;

fn main() {
    let handle = io::stdin();
    let mut buf = String::new();
    handle.read_line(&mut buf).unwrap();
    let N = buf.trim().parse::<usize>().unwrap() + 1;
    buf.clear();
    handle.read_line(&mut buf).unwrap();

    let sum = buf.split_whitespace().map(|s| s.parse::<usize>().unwrap()).sum::<usize>();

    if (sum + 3) % N != 1 {
        println!("3");
    } else {
        println!("2");
    }
}
