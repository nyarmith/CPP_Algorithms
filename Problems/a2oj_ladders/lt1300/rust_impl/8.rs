use std::io;

fn main() {
    let handle = io::stdin();
    let mut buf = String::new();
    handle.read_line(&mut buf).unwrap();
    let first_char = buf.chars().nth(0).unwrap();

    let first_char = first_char.to_uppercase();
    println!("{}{}",first_char, &buf[1..]);
}
