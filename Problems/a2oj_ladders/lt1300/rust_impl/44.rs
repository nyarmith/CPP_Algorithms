use std::io;
use std::collections::HashMap;

/* v1, faster but less about what we need to learn
struct TeamScore {
    name : String,
    score: usize
}

fn main() {
    let handle = io::stdin();
    let mut buf = String::new();
    handle.read_line(&mut buf).unwrap();

    let n : usize = buf.trim().parse().unwrap();

    buf.clear();
    handle.read_line(&mut buf).unwrap();
    let mut team1 = TeamScore{ name: buf.trim().to_string(), score: 1};
    buf.clear();
    let mut team2 = TeamScore{ name: String::new(), score: 0};
    for _ in 1..n {
        handle.read_line(&mut buf).unwrap();
        if buf.trim() == team1.name {
            team1.score += 1;
        } else {
            if team2.name.len() == 0 {
                team2.name = buf.trim().to_string();
            }
            team2.score += 1;
        }

        buf.clear();
    }

    if team1.score > team2.score {
        println!("{}", team1.name);
    } else {
        println!("{}", team2.name);
    }
}
*/

fn main() {
    let mut scores : HashMap<String, usize> = HashMap::new();
    let handle = io::stdin();
    let mut buf = String::new();
    handle.read_line(&mut buf).unwrap();

    let n : usize = buf.trim().parse().unwrap();
    buf.clear();

    for _ in 0..n {
        handle.read_line(&mut buf).unwrap();
        *scores.entry(buf.trim().to_string()).or_default() += 1;
        buf.clear();
    }

    let entry = scores.iter().max_by(|a,b| a.1.cmp(b.1)).unwrap();
    println!("{}", entry.0);
}
