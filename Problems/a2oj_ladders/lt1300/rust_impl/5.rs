use std::io;
use std::convert::TryFrom;


fn unique_vec(vec : &Vec<u8>) -> bool {
    let mut set = [false; 11];
    for c in vec {
        if set[*c as usize] {
            return false;
        }
        set[*c as usize] = true;
    }
    true
}

fn iter_digi_vec(vec : &mut Vec<u8>) {
    let mut ind : isize = isize::try_from(vec.len()-1).unwrap();
    let mut carry = true;
    while ind >= 0 {
        if carry {
            carry = false;
            vec[ind as usize] += 1;
            if vec[ind as usize] >= 10 {
                vec[ind as usize] = 0;
                carry = true;
            }
        }
        ind -= 1;
    }
}

fn main() -> io::Result<()> {
    let stdin = io::stdin();
    let mut buf = String::new();
    stdin.read_line(&mut buf)?;
    let mut digits : Vec<u8> = buf.trim().chars().map(|x| x.to_digit(10).unwrap() as u8).collect();

    iter_digi_vec(&mut digits);
    while !unique_vec(&digits) {
        iter_digi_vec(&mut digits);
    }

    let beautiful_year : String = digits.iter().map(|x| char::from_digit(*x as u32,10).unwrap()).collect();
    println!("{}", beautiful_year);
    Ok(())
}
