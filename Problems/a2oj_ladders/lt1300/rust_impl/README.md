Standouts from this collection... <br />

1 - Learn a less popular way of getting inputs, similar to file io -- read\_line into a string buffer, parse it, clear buffer, read again. If not cleared the fn will append. Very interesting choices. Also no warnings about out of array size accesses, I thought it'd be safer than that!

2 - Abs method/trait exists for basic types

3 - Standard way of handling char strings like we do in C is to collect the string's chars iterator into a Vec<char> type - you can do .rev() in the for .. in range thing (I guess the 1..2 thing is an iterator, so .rev() probably applies to all of them.

Also, apparently can't advance an iterator inside an iterator for loop (sad!)

4 - I there a better way to match substrings of 1 and 2 chars in rust? Ideally something like "match" with "a", "ab", "ac", as 3 cases

5 - Apparently functions without references or mut in rust take ownership rather than do copy. To make a copy, you have to explicitly implement the Copy trait, but even then a type with a reference cannot be copied. 
\\-> Basically think of it as similar to c# as there being Copy types and non-Copy types
\\-> e.g. String, Vec<T>, Box<T>, Rc<T>, Arc<T> are non-Copy types
\\-> e.g. primitive types like u32, i32, bool, char, etc., are all Copy types

7 - strings and chars have `is_ascii_upperacase()` and `to_ascii_uppercase()` methods

8 - also they have the more general `.to_uppercase()`, slices with &buf[1..] to get rest of a string seems janky but works

9 - recap but important: iterators have .map(...) methods

10 - .windows(n) gives you an iterating slice of n elements in an array, rolls over the whole array like a window. For disjoint sets of n (contiguous), see `.chunks(n)`

11 - iterators have `.step_by(i)` to iterate by some step size. very cool. Also strings have `.parse::<type>().unwrap()`

12 - format string number as binary "{:b}", to do a from binary string into a type, you can do `u64::from_str_radix(my_str, 2).unwrap()`

13 - Just another `step_by` usage for a prime sieve

14 - Tuple deconstruction but mutable, for initialization

15 - You can explicitly select template in a lot of methods, like `.sum::<f32>()`

16 - Cool destructuring of stdin trick (could probably be cleaner), use of .contains with reference to literal. Use of HashSet, step\_by. Pretty cool (but uses a lot of mem)

17 - Rust doesn't complain about putting, like, `{left+=0; right+=1;},` as a match statement expression, maybe should stop using caps or something, b/c I get the annoying `non_snake_case` error. Also I can just use `min(...)` if I have `use std::cmp::min`

18 - I feel like there's a lot more elegant way to do this. Also, I don't need to `try_into()` vec types, I can use `collect()`, and I need to remember to unwrap str parse attempts

19 - Directly using `iter.next()` to get an `Option<mytype>`

20 - Ultra memory efficient sorting of strings passed in, lol. Also `to_string()` returns a String, not a &str. Collecting into a `&[&str]` doesn't seem possible without first creating a Vec type to take the slice from :cry:

21 - Learned about dedup. Used it on a vector of string slices (after sorting) successfully. Probably more efficient than doing any conversion to numbers from strings, especially for this set size?

22 - iterators have a `.last()`, pretty cool

23 - You can only sort strings when you put the chars in a Vec. Could also use itertools to sort the iterator. Should try doing a char map on this one.

24 - Using a vec's `.extend()` method works on iterators

25 - Another meh destructuring -- need to find a better way to do this

26 - In non-returning matches (not let s = match ...), return stmt in-fact does return from the function (I guess just the non-semicolon statement would return a value from the match expression?)

27 - Use of zip for lexicographic comparison. Probably some kind of iter trick I can use along iwth a ternary expression to get my result more elegantly. -- update, I did that but maybe there's an even better way!

28 - iterator sum often wants its type to be qualified, even when I specify the type in some earlier iterator adapter so I'd assume it'd be the same

29 - `Some(_)` in match, str.find as a method that returns an option

30 - enumerate usage for index map of ordered number permutation. Big array initialization

31 - Very clean sum numbers in a string statement

32 - Nothing new really

33 - 

35 - Rust doesn't have integer sqrt, you've got to cast floats (makes sense tbh)

36 - Rust's windows and chunks are only methods of slices https://stackoverflow.com/questions/42134874/are-there-equivalents-to-slicechunks-windows-for-iterators-to-loop-over-pairs
\-> also, good example of how to sort just indices as a map to values
\-> There's a `.first()` and `.last()` method of vectors that return optional
\-> There's also at least a `.last()` for iterators, but I don't think this is a trait thing they're both implementing, just a fn with the same name

38 - Entry API for HashMap -- can get a reference to an entry that doesn't exist yet, then create it with a default or an (or\_insert), and then dereference it and modify that value
\-> e.g. `*map.entry(v).or_default() += 1;`

39 - Nothing rustic, but cool use of recursion with 3 vars here

40 - Probably some way to do this way cleaner and rusy-er

43 - First time using `iter.eq(..)`, chain it with `iter.rev()` to check if string is reversed. Wonder what the most efficient reversing algo is -- online everyone seems to use the iterator approach (obviously, it's very intuitive)

44 - Iterators have `max_by`, and hashmap has `max_by`, `max_by_key`, `min_by_key`
