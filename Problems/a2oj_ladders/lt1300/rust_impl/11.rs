use std::io;

fn main() {
    // we are generating primes until 50
    let mut prime_list = [true; 70];

    let mut i = 2;
    while i < 70 {
        if prime_list[i] {
            for j in (i..70).step_by(i).skip(1) {
                prime_list[j] = false;
            }
        } 
        i += 1;
    }

    // now, get stdin:
    let mut buf = String::new();
    let handle = io::stdin();
    handle.read_line(&mut buf).expect("reading line failed");
    let nums : Vec<usize> = buf.trim().split(" ").map(|s| s.parse::<usize>().unwrap()).collect();

    let first = nums[0];
    let second = nums[1];

    let mut ind = first + 1;

    while ind < second {
        if prime_list[ind] == true {
            println!("NO");
            return;
        }
        ind += 1;
    }
    if prime_list[ind] == true {
        println!("YES");
    } else {
        println!("NO");
    }
}
