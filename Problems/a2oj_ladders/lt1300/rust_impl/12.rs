use std::io;

fn main() {
    let handle = io::stdin();
    let mut buf = String::new();
    handle.read_line(&mut buf).expect("couldnt read first line");
    let num1 = u128::from_str_radix(buf.trim(), 2).unwrap();
    buf.clear();
    handle.read_line(&mut buf).expect("couldnt read first line");
    let num2 = u128::from_str_radix(buf.trim(), 2).unwrap();

    let my_str = format!("{:b}", num1 ^ num2);
    for _ in my_str.len()..buf.trim().len() {
        print!("0");
    }
    println!("{}",my_str);
}
