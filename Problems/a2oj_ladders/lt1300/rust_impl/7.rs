use std::io;

// oh no, more string processing in rust
// from stdin, print the corrected word s. If the given word s has strictly more uppercase letters, make the word written in the uppercase register, otherwise - in the lowercase one.

fn main() -> io::Result<()> {
    let handle = io::stdin();
    let mut buf = String::new();
    handle.read_line(&mut buf)?;
    buf = buf.trim().to_string();

    let mut uppercnt = 0;
    for c in buf.chars() {
        if c.is_ascii_uppercase() {
            uppercnt += 1;
        }
    }

    if uppercnt > buf.len()/2 {
        buf = buf.to_ascii_uppercase();
    } else {
        buf = buf.to_ascii_lowercase();
    }

    println!("{}", buf);

    Ok(())
}
