// given array nums, return running sum of an array as runningSum[i]  sum(nums[0]...nums[i])
impl Solution {
    pub fn running_sum(nums: Vec<i32>) -> Vec<i32> {
        let mut ret = Vec::new();
        ret.push(nums[0]);
        for i in 1..nums.len() {
            ret.push(ret[i-1] + nums[i]);
        }
        ret
    }
}
