// given an array of integers nums, calculate the pivot index of this array, that is
// an index where sum of elements to the left are equal to sum of elements to the right
impl Solution {
    pub fn pivot_index(nums: Vec<i32>) -> i32 {
        let mut sum : i32 = nums.iter().sum();
        let mut lsum : i32 = 0;

        for i in 0..nums.len() {            
            sum -= nums[i];
            if lsum == sum {
                return i as i32;
            }
            lsum += nums[i];
        }
        -1 as i32
    }
}
