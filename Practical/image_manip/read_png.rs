use std::fs;
use std::str;
use std::convert::TryInto;



fn valid_png_header(data: &Vec<u8>) -> bool {
    let header = [0x89, b'P', b'N', b'G', 0x0D, 0x0A, 0x1A, 0x0A];
    header == data[0..8]
}


fn crc32(data: &[u8]) -> u32 {
    let mut crc: u32 = 0x00;
    for chunk in data.chunks(4) {
        let current = u32::from_be_bytes(chunk.try_into().unwrap());
        // ok I'm not doing this for now https://stackoverflow.com/questions/2587766/how-is-a-crc32-checksum-calculated
        // and https://web.archive.org/web/20130715065157/http://gnuradio.org/redmine/projects/gnuradio/repository/revisions/1cb52da49230c64c3719b4ab944ba1cf5a9abb92/entry/gr-digital/lib/digital_crc32.cc
    }
    0
}

#[derive(Debug)]
enum ColorType {
    Greyscale,
    Truecolor,
    IndexedColor,
    GreyscaleAlpha,
    TruecolorAlpha
}

struct IHDR {
    width: u32,
    height: u32,
    bit_depth: u8,
    color_type: ColorType,
    compression: u8,
    filter: u8,
    interlace: u8
}

const CHUNK_TYPE_IHDR : [u8;4] = [b'I', b'H', b'D', b'R'];
fn read_ihdr(data: &[u8]) -> IHDR {
    let width = u32::from_be_bytes(data[..4].try_into().unwrap());
    let height = u32::from_be_bytes(data[4..8].try_into().unwrap());
    let bit_depth = data[8];
    let color_type : ColorType = match data[9] {
        0 => ColorType::Greyscale,
        2 => ColorType::Truecolor,
        3 => ColorType::IndexedColor,
        4 => ColorType::GreyscaleAlpha,
        6 => ColorType::TruecolorAlpha,
        _ => panic!("Invalid color type {} in IHDR!", data[8])
    };

    // test validity of bit_depth with color type
    match (&color_type, bit_depth) {
        (ColorType::Greyscale,      n) =>  assert!( [1,2,4,8,16].contains(&n), "Invalid depth for {:?}, '{}'", color_type, bit_depth),
        (ColorType::Truecolor,      n) =>  assert!(       [8,16].contains(&n), "Invalid depth for {:?}, '{}'", color_type, bit_depth),
        (ColorType::IndexedColor,   n) =>  assert!( [1,2,4,8]   .contains(&n), "Invalid depth for {:?}, '{}'", color_type, bit_depth),
        (ColorType::GreyscaleAlpha, n) =>  assert!(       [8,16].contains(&n), "Invalid depth for {:?}, '{}'", color_type, bit_depth),
        (ColorType::TruecolorAlpha, n) =>  assert!(       [8,16].contains(&n), "Invalid depth for {:?}, '{}'", color_type, bit_depth)
    }

    let compression = data[10];
    let filter = data[11];
    let interlace = data[12];

    println!("width: {}, height: {}", width, height);
    println!("color type: {:?}, bit depth: {}", color_type, bit_depth);
    println!("compression method: {}, filter_method: {}, interlace_method: {}",
             compression, filter, interlace);

    IHDR {width, height, bit_depth, color_type, compression, filter, interlace}
}

const CHUNK_TYPE_SRGB : [u8;4] = [b's', b'R', b'G', b'B'];
fn read_srgb(data: &[u8]) {
    let intent = data[0];
    match intent {
        0x0 => println!("Perceptual"),
        0x1 => println!("Relative colorimetric"),
        0x2 => println!("Saturation"),
        0x3 => println!("Absolute colorimetric"),
        _ => println!("Unknown srgb chunk intent")
    }
}

const CHUNK_TYPE_GAMA : [u8;4] = [b'g', b'A', b'M', b'A'];
fn read_gama(data: &[u8]) {
    let gamma = u32::from_be_bytes(data[..4].try_into().unwrap());
    println!("Gamma : {}", (gamma as f64) / 1000.0);
}

const CHUNK_TYPE_PHYS : [u8;4] = [b'p', b'H', b'Y', b's'];
fn read_phys(data: &[u8]) {
    let ppu_x = u32::from_be_bytes(data[..4].try_into().unwrap());
    let ppu_y = u32::from_be_bytes(data[4..8].try_into().unwrap());
    let unit = data[8];
    println!("Pixels Per Unit, X-Axis : {}", ppu_x);
    println!("Pixels Per Unit, Y-Axis : {}", ppu_y);
    match unit {
        0 => println!("Unit type unknown"),
        1 => println!("Unit type meter"),
        _ => println!("Unit type very unknown!"),
    }
}

const CHUNK_TYPE_EXIF : [u8;4] = [b'e', b'X', b'I', b'f'];
fn read_exif(data: &[u8]) {
    // this is terrible, may skip and save for final boss: https://exiftool.org/TagNames/EXIF.html
    println!("skipping exif!");
}

const CHUNK_TYPE_TEXT : [u8;4] = [b't', b'E', b'X', b't'];
fn read_text(data: &[u8], len: usize) {
    let nul_ind = data.iter().position(|&c| c == b'\0').expect("Error finding null terminator in tEXt chunk!");
    println!("keyword: {}", String::from_utf8_lossy(&data[..nul_ind]));
    let o = nul_ind + 1;
    println!("text: {}", String::from_utf8_lossy(&data[o..len]));
}

// the chunk containing actual image data
const CHUNK_TYPE_IDAT : [u8;4] = [b'I', b'D', b'A', b'T'];
fn read_idat(data: &[u8], ihdr: &IHDR) {

}

fn read_chunk(data: &[u8], ihdr: &mut IHDR) -> Option<usize> {
    let chunk_len = u32::from_be_bytes(data[..4].try_into().unwrap()) as usize;
    let chunk_type : [u8;4] = data[4..8].try_into().unwrap();

    let data = &data[8..];
    let chunk_data = &data[..chunk_len];
    let chunk_crc = &data[chunk_len..(chunk_len+4)];

    // check crc before further processing, which is computer over chunk_data + chunk_type (no len)

    match chunk_type {
        CHUNK_TYPE_IHDR => {
            *ihdr = read_ihdr(chunk_data);
        }
        CHUNK_TYPE_SRGB => {
            read_srgb(chunk_data);
        }
        CHUNK_TYPE_GAMA => {
            read_gama(chunk_data);
        }
        CHUNK_TYPE_PHYS => {
            read_phys(chunk_data);
        }
        CHUNK_TYPE_EXIF => {
            read_exif(chunk_data);
        }
        CHUNK_TYPE_TEXT => {
            read_text(chunk_data, chunk_len);
        }
        CHUNK_TYPE_IDAT => {
            read_idat(chunk_data, &ihdr);
        }
        _ => {
            println!("Error chunk type: '{}' not handled", str::from_utf8(&chunk_type).ok()?);
            return None
        }
    }
    Some(12 + chunk_len)
}

fn main() -> Result<(), Box<dyn std::error::Error + 'static>> {
    let mut data = fs::read("sample.png")?;
    let is_valid = valid_png_header(&data);
    println!("is header valid?: {}", is_valid);
    if !is_valid {
        return Ok(())
    }

    let mut data = &data[8..];
    let mut ihdr = IHDR{width: 0, height: 0, bit_depth: 0, color_type: ColorType::Greyscale, compression: 0, filter: 0, interlace: 0};
    loop {
        match read_chunk(data, &mut ihdr) {
            Some(n) => {
                println!("{} bytes read", n);
                data = &data[n..];
            }
            None => break
        }
    }
    
    Ok(())
}

