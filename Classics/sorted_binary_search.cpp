#include <iostream>
#include <vector>
#include <string>
#include <cstdint>
#include <cstdio>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <set>
#include <map>
#include <utility>
#include <queue>
#include <sstream>
#define ll long long


void display_round(std::vector<int> &arr, int i, int j, int mid, int round) {
    // convert nums to string and get max width
    int spacing = std::to_string(*arr.rbegin()).size();
    // print with | between the specific range
    std::stringstream second_line;

    std::cout << "Round " << round << ", i=" << i << ",j=" << j << ", mid =  " << mid << "\n";

    for (int idx=0; idx < arr.size(); ++idx) {

        if (idx == i) {
            std::cout << "|";
            second_line << "|";
        }
        else if (idx - 1 != j)
        {
            std::cout << " ";
            second_line << " ";
        }

        auto idx_str = std::to_string(idx);
        std::cout << idx_str << std::string(" ", spacing - idx_str.length());

        auto num_str = std::to_string(arr[idx]);
        second_line << num_str << std::string(" ", spacing - num_str.length());

        if (idx == j) {
            std::cout << "|";
            second_line << "|";
        }
    }

    std::cout << "\n";

    std::cout << second_line.str() << "\n";
}

// return index where it's at, or -1
int graphical_log_find(std::vector<int>& arr, int i, int j, int val, int round)
{
  int middle = (i+j)/2;
  display_round(arr, i, j, middle, round);
  round++;

  if (i > j || round > 15){
    return -1;
  }


  if (arr[middle] == val) {
      return middle;
  } else if (arr[middle] > val) {
      return graphical_log_find(arr, i, middle-1, val, round);
  } else {
      return graphical_log_find(arr, middle+1, j, val, round);
  }
}

int main(int argc, char** argv)
{
  if (argc < 2){
    std::cout << "Give me an int array, e.g. 1 2 3 4\n";
  }
  //merge-sort stdin
  std::vector<int> sorted_arr = {0,1,2,3,5,10,11,23,24,25,30};


  std::cout << "\nFinding 2\n";
  std::cout << graphical_log_find(sorted_arr, 0, sorted_arr.size()-1, 2, 1) << "\n";

  std::cout << "\nFinding 23\n";
  std::cout << graphical_log_find(sorted_arr, 0, sorted_arr.size()-1, 23, 1) << "\n";

  std::cout << "\nFinding 30\n";
  std::cout << graphical_log_find(sorted_arr, 0, sorted_arr.size()-1, 30, 1) << "\n";

  std::cout << "\nFinding 0\n";
  std::cout << graphical_log_find(sorted_arr, 0, sorted_arr.size()-1, 0, 1) << "\n";

  std::cout << "\nFinding -1\n";
  std::cout << graphical_log_find(sorted_arr, 0, sorted_arr.size()-1, -1, 1) << "\n";
  
  std::cout << "\nFinding 31\n";
  std::cout << graphical_log_find(sorted_arr, 0, sorted_arr.size()-1, 31, 1) << "\n";

  std::cout << "\nFinding 1\n";
  std::cout << graphical_log_find(sorted_arr, 0, sorted_arr.size()-1, 1, 1) << "\n";

  return 0;
}
