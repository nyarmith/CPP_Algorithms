#include <iostream>
#include <ostream>
#include <vector>
#include <algorithm>

struct ImplicitSegmentTree {
    std::vector<int> tree;
    std::vector<int> lo;
    std::vector<int> hi;
    std::vector<int> dt;

public:
    ImplicitSegmentTree(size_t n) {
        tree = lo = hi = dt = std::vector<int>(2*n+1); //extra 2n for a layer below, considering the most unbalanced config of a tree
        
        init(1, 0, n-1);
    }

    ImplicitSegmentTree(std::vector<int> initial) : ImplicitSegmentTree(initial.size()) {
        // with n leaves, we have n-1 parent nodes
        int offset = initial.size();
        for (size_t i=0; i<initial.size(); ++i) {
            tree[offset+i] = initial[i];
        }

        for (int i=offset-1; i>=0; --i) {
            tree[i] = std::min(tree[i*2], tree[i*2+1]);
        }
    }

    void add(int i, int j, int val)
    {
        add(1, i, j, val);
    }

    int minimum(int i, int j) {
        return minimum(1, i, j);
    }

private:

    void prop(int i)
    {
        dt[2*i] += dt[i];
        dt[2*i + 1] += dt[i];
        dt[i] = 0;
    }

    void update(int i)
    {
        tree[i] = std::min(tree[2*i] + dt[2*i], tree[2*i+1] + dt[2*i+1]);
    }


    void init(int i, int a, int b) {

        lo[i] = a;
        hi[i] = b;
        //tree[i] = 0;

        if (a == b)
            return;

        int m = (a+b)/2;
        init(2*i, a, m);
        init(2*i+1, m+1, b);
    }

    void add(int i, int a, int b, int val)
    {
        // not covered
        if (b < lo[i] || hi[i] < a)
            return;

        // complete cover
        if (a <= lo[i] && hi[i] <= b)
        {
            dt [i] += val;
            return;
        }

        // partial cover
        prop(i);
        add(2*i, a, b, val);
        add(2*i+1, a, b, val);

        update(i);
    }

    int minimum(int i, int a, int b)
    {
        // not covered
        if (b < lo[i] || hi[i] < a)
            return 0x7FFFFFFF; // int max for invalid ranges

        // complete cover
        if (a <= lo[i] && hi[i] <= b)
        {
            return tree[i] + dt[i];
        }

        // partial cover
        prop(i);

        int minLeft = minimum(2*i, a, b);
        int minRight = minimum(2*i+1, a, b);

        update(i);

        return std::min(minLeft, minRight);
    }
};

int main() {
    ImplicitSegmentTree segtree(16);
    segtree.add(1,2,5);
    segtree.add(1,3,2);
    std::cout << segtree.minimum(1,1) << "\n";
    std::cout << segtree.minimum(1,1) << "\n";
    std::cout << segtree.minimum(1,3) << "\n";
    std::cout << segtree.minimum(1,4) << "\n";
    std::cout << segtree.minimum(1,4) << "\n";

    // let's see if my vec decl worked
    ImplicitSegmentTree segtree2({5,10,4,3,2,1,1,2});
    std::cout << "With Vec Init:" << "\n";
    std::cout << "0"   << ": " << segtree2.minimum(0,0) << "\n";
    std::cout << "1"   << ": " << segtree2.minimum(1,1) << "\n";
    std::cout << "2"   << ": " << segtree2.minimum(2,2) << "\n";
    std::cout << "3"   << ": " << segtree2.minimum(3,3) << "\n";
    std::cout << "4"   << ": " << segtree2.minimum(4,4) << "\n";
    std::cout << "5"   << ": " << segtree2.minimum(5,5) << "\n";
    std::cout << "6"   << ": " << segtree2.minimum(6,6) << "\n";
    std::cout << "7"   << ": " << segtree2.minimum(7,7) << "\n";
    std::cout << "8"   << ": " << segtree2.minimum(8,8) << "\n";
    std::cout << "6,8" << ": " << segtree2.minimum(6,8) << "\n";
    std::cout << "1,8" << ": " << segtree2.minimum(1,8) << "\n";
    segtree2.add(4,7,10);

    // let's see if my vec decl worked
    ImplicitSegmentTree segtree3({5,10,4,3,2,1});
    std::cout << "With nonpow8 size Vec Init:" << "\n";
    std::cout << "0"   << ": " << segtree3.minimum(0,0) << "\n";
    std::cout << "1"   << ": " << segtree3.minimum(1,1) << "\n";
    std::cout << "2"   << ": " << segtree3.minimum(2,2) << "\n";
    std::cout << "3"   << ": " << segtree3.minimum(3,3) << "\n";
    std::cout << "4"   << ": " << segtree3.minimum(4,4) << "\n";
    std::cout << "5"   << ": " << segtree3.minimum(5,5) << "\n";
    std::cout << "6"   << ": " << segtree3.minimum(6,6) << "\n";
    std::cout << "7"   << ": " << segtree3.minimum(7,7) << "\n";
    std::cout << "6,8" << ": " << segtree3.minimum(6,8) << "\n";
    std::cout << "1,8" << ": " << segtree3.minimum(1,8) << "\n";
    segtree3.add(5,5,10);
}
