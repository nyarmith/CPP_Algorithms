#include <iostream>
#include <unordered_set>
#include <unordered_map>
#include <queue>


// bidirectional paths
struct Edge {
    int x, y;
    int weight;

    Edge(int X, int Y, int wgt) {
        if (X < Y) std::swap(X,Y);
        x = X;
        y = Y;
        weight = wgt;
    }

    bool operator<(const Edge &o) const { return weight < o.weight; }
    bool operator>(const Edge &o) const { return weight > o.weight; }
    bool operator==(const Edge &o) const { return x==o.x && y == o.y; }
};

struct EdgeHasher {
    size_t operator()(const Edge& v) const {
        return (v.x*(v.x+1)+v.y);
    };
};

int main() {
    std::vector<Edge> edges = { {0,1,2}, {1,2,1}, {0,2,2}, {2,3,2}, {1,3,1}, {3,1,3}, /*{0,4,1}, {4,3,1} -- extra shortcut to test */ };
    /* graph looks like
    *
    *        3
    *      // \
    *  3,1//   \ 2
    *    //  1  \
    *    1------2
    *     \    /
    *    2 \_ / 2
    *        0
    *   
    *
    */
    
    // now we want to find the shortest path between 0 and 3
    //std::unordered_set<Edge, EdgeHasher> edges;
    std::unordered_map<int, std::unordered_map<int,int>> points;
    std::unordered_map<int,int> visited;
    std::priority_queue<Edge, std::vector<Edge>, std::greater<Edge>> paths;

    for (auto &a : edges) {
        points[a.x][a.y] = a.weight;
        points[a.y][a.x] = a.weight;
    }

    int start = 0;
    int end = 3;
    int min_path = 0;

    for (auto &pair : points[start]) {
        paths.push({start, pair.first, pair.second});
    }

    visited[0] = 0;
    while (paths.size() > 0) {
        auto cur_path = paths.top();  // get our shortest path we've found crawling out from the start
        paths.pop();
        visited[cur_path.x] = cur_path.weight; // shortest path to the element found
        if (cur_path.x  == end) {  // note this check only works because we start from 0, if it was not we'd have to check both
            min_path = cur_path.weight;
            break;
        }

        // add the paths connected to this
        for (auto &next : points[cur_path.x]) {
            if (visited.find(next.first) == visited.end())
                paths.push({cur_path.y, next.first, cur_path.weight + next.second});
        }
    }

    std::cout << min_path << "\n";
}
