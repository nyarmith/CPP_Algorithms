#include <iostream>
#include <vector>
#include <string>
#include <cstdint>
#include <cstdio>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <set>
#include <map>
#include <utility>
#include <queue>
#define ll long long

// a "binary indexed tree" for dealing with range queries
class fenwick {
    std::vector<int> tree;

public:
    fenwick() { tree.push_back(0); } // initial dummy element for clean logic

    fenwick(const std::vector<int> &data) : fenwick() {
        // with initial endowment of data
        tree.insert(tree.end(), data.begin(), data.end());
        /* nlogn implementation
        tree = std::vector<int>(data.size()+1);
        for (size_t i=1; i<tree.size(); ++i) {
            tree[i] = data[i-1];
            int off = (i&-i) - 1;
            while (off) {
                tree[i] += data[i-off-1];
                off--;
            }
        }
        */
        

        // O(n) implementation, by using linear construction
        //tree = std::vector<int>(data.size()+1);
        //tree[0] = 0;
        //for (int i=1; i<=data.size(); ++i) {
        //    tree[i] += data[i-1];
        //    // propogate this i to its parent early
        //    //size_t p = (i | (i+1)) + 1;// alternative lsb thing, for indexes starting with 0
        //    size_t p = i + (i & -i);
        //    if (p < tree.size()) tree[p] += tree[i];
        //}
    }

    // return sum of elements from (0,idx) of tree
    int get_sum(size_t idx) {
        int ret = 0;
        while (idx){
            ret += tree[idx];
            idx -= idx & -idx;
        }
        return ret;
    }

    // get single frequency at a point
    int get_freq(int idx) {
        int ret = tree[idx];
        if (idx == 0) return ret;
        int common = idx - (idx & -idx);
        int y = idx-1;
        while (y != common) {
            ret -= tree[y];
            y -= y & -y;
        }

        return ret;
    }

    // populate with new element
    void push(int element) {
        int idx = tree.size();
        int iter_idx = idx & -idx;
        tree.push_back(element);
        for (int i=1; i<iter_idx; i++){
            tree[idx] += get_freq(idx - i);
        }
    }

    void add(size_t idx, int val) {
        while (idx < tree.size()) {
            tree[idx] += val;
            idx += idx & -idx;
        }
    }

    friend std::ostream& operator<<(std::ostream& os, const fenwick &f) {
        for (int i=1; i<f.tree.size(); ++i) {
            os << f.tree[i] << " ";
        }
        return os;
    }
};

// Update range, query point
class reverse_fenwick {
    std::vector<int> tree;

public:
    //reverse_fenwick() { tree.push_back(0); } // initial dummy element for clean logic
                                             //
    reverse_fenwick(int size) { tree = std::vector<int>(size+1); } // initial dummy element for clean logic

    reverse_fenwick(const std::vector<int> &data) : reverse_fenwick(data.size()) {
        // nlogn implementation
        tree = std::vector<int>(data.size()+1);
        for (size_t i=1; i<tree.size(); ++i) {
            update(i,i,data[i-1]);
        }

        // not an O(n) implementation, by using linear construction
        /*tree = std::vector<int>(data.size()+1);
        tree[0] = 0;
        for (int i=1; i<=data.size(); ++i) {
            tree[i] += data[i-1];
            if (i>1) {
                tree[i] -= tree[i-1];
                size_t j = (i-1) + ((i-1) & -(i-1));
                tree[j] -= tree[i-1];
            }
            // propogate this i to its parent early
            //size_t p = (i | (i+1)) + 1;// alternative lsb thing, for indexes starting with 0
            size_t p = i + (i & -i);
            if (p < tree.size()) tree[p] += tree[i];
        }*/
    }

    // return sum of elements from (0,idx) of tree
    int get_freq(size_t idx) {
        int ret = 0;
        while (idx){
            ret += tree[idx];
            idx -= idx & -idx;
        }
        return ret;
    }

    // get sum at a point
    int get_sum(int idx) {
        int ret = tree[idx];
        if (idx == 0) return ret;
        int common = idx - (idx & -idx);
        int y = idx-1;
        while (y != common) {
            ret -= tree[y];
            y -= y & -y;
        }

        return ret;
    }

    // populate with new element
    void push(int element) {
        tree.push_back(element);
    }

    void update(size_t l, size_t r, int val) {
        update(l, val);
        update(r+1, -val);
    }

public:
    friend std::ostream& operator<<(std::ostream& os, const reverse_fenwick &f) {
        for (int i=1; i<f.tree.size(); ++i) {
            os << f.tree[i] << " ";
        }
        return os;
    }

private:
    void update(size_t idx, int val) {
        while (idx < tree.size()) {
            tree[idx] += val;
            idx += idx & -idx;
        }
    }
};


class super_fenwick {
    std::vector<int> B1;
    std::vector<int> B2;

public:
    //reverse_fenwick() { tree.push_back(0); } // initial dummy element for clean logic
                                             //
    super_fenwick(int size) { B1 = B2 = std::vector<int>(size+1); } // initial dummy element for clean logic

    super_fenwick(const std::vector<int> &data) : super_fenwick(data.size()+1) {
        /* nlogn implementation
        tree = std::vector<int>(data.size()+1);
        for (size_t i=1; i<tree.size(); ++i) {
            tree[i] = data[i-1];
            int off = (i&-i) - 1;
            while (off) {
                tree[i] += data[i-off-1];
                off--;
            }
        }
        */
        

        // O(n) implementation, by using linear construction
        //tree = std::vector<int>(data.size()+1);
        //tree[0] = 0;
        //for (int i=1; i<=data.size(); ++i) {
        //    tree[i] += data[i-1];
        //    tree[i] -= tree[i-1]; // range update, individual query
        //    // propogate this i to its parent early
        //    //size_t p = (i | (i+1)) + 1;// alternative lsb thing, for indexes starting with 0
        //    size_t p = i + (i & -i);
        //    if (p < tree.size()) tree[p] += tree[i];
        //}
    }

    // return sum of elements from (0,idx) of tree
    int get_freq(size_t idx) {
        return 0;
    }

    // get sum at a point
    int get_sum(int idx) {
        return 0;
    }

    // populate with new element, very simple in reverse case
    //void push(int element) {
    //    tree.push_back(element);
    //}

    void update(size_t l, size_t r, int val) {
        return;
    }

public:
    friend std::ostream& operator<<(std::ostream& os, const super_fenwick &f) {
        for (int i=1; i<f.B1.size(); ++i) {
            os << f.B1[i] << " ";
        }
        os << "\n";
        for (int i=1; i<f.B1.size(); ++i) {
            os << f.B2[i] << " ";
        }
        return os;
    }

private:
    void update(size_t idx, int val) {
    }
};


// templated version, but one dimension
int main(){

    fenwick first_bit({1,0,2,1,1,3,0,4,2,5,2,2,3,1,0,2});
    std::cout << first_bit << "\n";
    std::cout << "sum from 3 to 12: " << first_bit.get_sum(12) - first_bit.get_sum(2) << "\n";
    std::cout << "Freq at idx 6, should be 3: " << first_bit.get_freq(6) << "\n";

    // now, constructing the same fenwick tree, but with iteration
    fenwick second_bit;
    std::vector<int> dat {1,0,2,1,1,3,0,4,2,5,2,2,3,1,0,2};
    for (auto d : dat)
        second_bit.push(d);
    std::cout << second_bit << "\n";

    // now, try reverse fenwick tree and see if outputs are the saem
    //reverse_fenwick third_bit({1,0,2,1,1,3,0,4,2,5,2,2,3,1,0,2});
    reverse_fenwick third_bit(16);
    third_bit.update(1,13,3);
    third_bit.update(13,15,2);
    third_bit.update(12,12,1);
    std::cout << "Freq at idx 12, should be 4: " << third_bit.get_freq(12) << "\n";
    std::cout << "Freq at idx 13, should be 5: " << third_bit.get_freq(13) << "\n";
    std::cout << "Freq at idx 3, should be 3: " << third_bit.get_freq(3) << "\n";
    std::cout << "Freq at idx 16, should be 0: " << third_bit.get_freq(16) << "\n";
    std::cout << "Freq at idx 15, should be 2: " << third_bit.get_freq(15) << "\n";
    std::cout << "\nOk, now that that works, let's give an initial endowment and then do some range operations\n";

    reverse_fenwick fourth_bit({1,0,2,1,1,3,0,4,2,5,2,2,3,1,0,2});
    std::cout << "Freq at idx 6, should be 3: " << fourth_bit.get_freq(6) << "\n";
    std::cout << "Freq at idx 5, should be 1: " << fourth_bit.get_freq(5) << "\n";

}
